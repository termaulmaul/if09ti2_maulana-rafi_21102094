# Repository Overview

This repository contains various practice projects and assignments developed by Maulana Rafi. Each folder represents a specific practice or assignment, showcasing different aspects of web development, programming, and software engineering.

## Identity

- **Nama:** Maulana Rafi
- **NIM:** 21102094
- **Kelas:** TI2_IF09

## Folder Descriptions:

1. **build**
   - Last Commit: 3 months ago
   - Description: Pembuatan folder build untuk hosting.

2. **images**
   - Last Commit: 3 months ago
   - Description: Penambahan folder images, file images.html, dan file img_chania.jpg.

3. **practice1**
   - Last Commit: 3 months ago
   - Description: Perbaikan Menggunakan Folder.

4. **practice2**
   - Last Commit: 3 months ago
   - Description: Update all HTML files with Tailwind CSS.

5. **practice3**
   - Last Commit: 2 months ago
   - Description: Update practice3/index.html.

6. **practice4**
   - Last Commit: 1 month ago
   - Description: Adding practice9 folder and all dependencies.

7. **practice6**
   - Last Commit: 2 months ago
   - Description: Update index.html.

8. **practice8**
   - Last Commit: 1 month ago
   - Description: Update hello.php.

9. **practice9**
   - Last Commit: 1 month ago
   - Description: Update index.php, koneksi.php, dan simpan_mahasiswa.php.

10. **practice_laravel**
    - Last Commit: 12 hours ago
    - Description: Update for practice12.

11. **sprint1**
    - Last Commit: 1 month ago
    - Description: Adding practice9 folder and all dependencies.

12. **.DS_Store**
    - Last Commit: 1 month ago
    - Description: Add practice 8.

13. **.gitlab-ci.yml**
    - Last Commit: 3 months ago
    - Description: Add .gitlab-ci.yml file.

14. **README.md**
    - Last Commit: 1 month ago
    - Description: Update README.md.

15. **Tugas3_4_Maulana Rafi_practice4.zip**
    - Last Commit: 2 months ago
    - Description: Update.

## Note
- This repository is a collection of practice projects and assignments.
- Please refer to individual folders for detailed information on each project.
- Contributions and improvements are welcome! Feel free to explore and enhance the projects as needed.
