<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminLTEController;
use App\Http\Controllers\AdminLTEStudentController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/student/create', 'App\Http\Controllers\StudentController@create')
->name('student.create')->middleware('login_auth');

Route::post('/student', 'App\Http\Controllers\StudentController@store')
->name('student.store')->middleware('login_auth');

Route::get('/student', 'App\Http\Controllers\StudentController@index')
->name('student.index')->middleware('login_auth');

Route::get('/student/{student}', 'App\Http\Controllers\StudentController@show')
->name('student.show')->middleware('login_auth');

Route::get('/student/{student}/edit', 'App\Http\Controllers\StudentController@edit')
->name('student.edit')->middleware('login_auth');

Route::patch('/student/{student}', 'App\Http\Controllers\StudentController@update')
->name('student.update')->middleware('login_auth');

Route::delete('/student/{student}', 'App\Http\Controllers\StudentController@destroy')
->name('student.destroy')->middleware('login_auth');

Route::get('/login', 'App\Http\Controllers\AdminController@index')
->name('login.index');

Route::get('/logout', 'App\Http\Controllers\AdminController@logout')
->name('login.logout');

Route::post('/login', 'App\Http\Controllers\AdminController@process')
->name('login.process');

Route::get('/adminlte/index', [AdminLTEController::class,'index'])
->name('adminlte.index');

Route::get('/adminlte/student/create', [AdminLTEStudentController::class,'create'])
->name('adminlte.student.create');
